#ifndef BOARD_HPP
#define BOARD_HPP

#include <iostream>
#include <vector>

enum Player { PLAYER_NONE, PLAYER_WHITE, PLAYER_BLACK, PLAYER_DRAW };

class Board {
    private:
        int _ni;
        int _nj;
        std::vector<Player> _data;

    public:
        Board(int ni, int nj);

        int ni() const;
        int nj() const;

        Player get(int i, int j) const;
        void set(int i, int j, Player p);

    protected:
        int ijToK(int i, int j) const;
};

#endif

