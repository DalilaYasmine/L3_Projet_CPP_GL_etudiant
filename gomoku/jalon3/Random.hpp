#ifndef RANDOM_HPP
#define RANDOM_HPP

#include <random>

class Random {
    private:
        std::mt19937_64 _engine;
        std::uniform_real_distribution<double> _distribution;

    public:
        Random();
        Random(const Random &) = delete;
        int operator()(int nMax);
};

#endif

