#include <chrono>
#include <string>

#include <Wt/Dbo/Dbo.h>
#include <Wt/Dbo/backend/Sqlite3.h>
#include <Wt/Dbo/WtSqlTraits.h>
#include <Wt/WDateTime.h>

using namespace Wt;
using namespace std;
auto now = chrono::system_clock::now;

// définit un type de données à stocker dans une table
struct GameResult {
    WDateTime date;
    int winner;
    template<class Action> void persist(Action & a) {
        Dbo::field(a, date, "date");
        Dbo::field(a, winner, "winner");
    }
};

int main(int argc, char ** argv) {

    // ouvre une base de données sqlite et active la table
    Dbo::Session session;
    session.setConnection(make_unique<Dbo::backend::Sqlite3>("toto.db"));
    session.mapClass<GameResult>("gameresult");

    // si la base n'existe pas, crée les tables et ajoute des données de test
    try {
        session.createTables();
        Dbo::Transaction transaction(session);
        session.add(make_unique<GameResult>(GameResult{WDateTime(now()), 1}));
        session.add(make_unique<GameResult>(GameResult{WDateTime(now()), 2}));
        session.add(make_unique<GameResult>(GameResult{WDateTime(now()), 1}));
    }
    catch (Dbo::Exception & e) {
        cerr << "Dbo::Exception: " << e.what() << endl;
    }

    // effectue une requête select qui retourne une seule ligne de données
    {
        Dbo::Transaction transaction(session);
        int count = session.query<int>("select count(1) from GameResult")
            .where("winner = ?").bind("1");
        cout << "nb white = " << count << endl;
    }

    // effectue une requête select qui retourne plusieurs lignes de données
    {
        Dbo::Transaction transaction(session);
        Dbo::collection<Dbo::ptr<GameResult>> grs = session.find<GameResult>();
        cout << "nb grs = " << grs.size() << endl;
        for (const Dbo::ptr<GameResult> & gr : grs)
            cout << gr->date.toString() << " -> " << gr->winner << endl;
    }

    return 0;
}

