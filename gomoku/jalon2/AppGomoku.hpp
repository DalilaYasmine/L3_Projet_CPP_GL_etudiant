#ifndef APP_GOMOKU_HPP
#define APP_GOMOKU_HPP

#include "BoardWidget.hpp"

#include <Wt/WApplication.h>
#include <Wt/WText.h>

class AppGomoku : public Wt::WApplication {
    private:
        Wt::WText * _myText;
        int _counter;

    public:
        explicit AppGomoku(const Wt::WEnvironment & env);
        void count();
};

#endif

