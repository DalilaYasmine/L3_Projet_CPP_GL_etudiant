#include "AppGomoku.hpp"
#include "BoardWidget.hpp"

#include <Wt/WContainerWidget.h>
#include <Wt/WPushButton.h>
#include <Wt/WTemplate.h>

using namespace std;
using namespace Wt;

const string APP_GOMOKU_HTML = R"(
    <h1> Gomoku </h1>
    <p> ${my-text} </p>
    <p> ${my-button} </p>
    <div> ${board-widget} </div> 
)";

AppGomoku::AppGomoku(const WEnvironment & env) : WApplication(env), _counter(-1) {
    // add css styles
    auto & css = instance()->styleSheet();
    css.addRule("body", R"(background-color: beige)");

    // add html template
    auto t = root()->addWidget(make_unique<WTemplate>(APP_GOMOKU_HTML));

    // bind html elements to c++ variables
    _myText = t->bindWidget("my-text", make_unique<WText>());
    t->bindWidget("board-widget", make_unique<BoardWidget>(*this));

    // connect elements to callback functions
    auto countButton = t->bindWidget("my-button", make_unique<WPushButton>("Count"));
    countButton->clicked().connect(this, &AppGomoku::count);

    count();
}

void AppGomoku::count() {
    _counter++;
    _myText->setText(to_string(_counter));
}

