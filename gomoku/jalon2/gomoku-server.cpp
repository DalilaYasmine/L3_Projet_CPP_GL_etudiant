#include "AppGomoku.hpp"

#include <Wt/WServer.h>

int main(int argc, char ** argv) {
    try {
        Wt::WServer server(argc, argv, WTHTTP_CONFIGURATION);

        server.addEntryPoint(Wt::EntryPointType::Application, 
                [](const Wt::WEnvironment & env)
                { return std::make_unique<AppGomoku>(env); },
                "/");

        server.run();
    } 
    catch (Wt::WServer::Exception & e) {
        std::cerr << "Wt::WServer::Exception: " << e.what() << std::endl;
    } 
    catch (std::exception & e) {
        std::cerr << "std::exception: " << e.what() << std::endl;
    }
    return 0;
}

